module codeberg.org/rohandebsarkar/website

go 1.19

require (
	github.com/FortAwesome/Font-Awesome v0.0.0-20221115183454-96cafbd73ec4 // indirect
	github.com/rohandebsarkar/academic-folio v0.0.0-20230518042141-953f4d30e3dd // indirect
)
